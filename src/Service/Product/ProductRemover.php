<?php

namespace App\Service\Product;

use App\EventListener\Events\ProductEvent;
use App\EventListener\Events\ProductEvents;
use App\Model\Product\Product;
use App\Model\Product\Repository\ProductRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProductRemover
{
    private $repository;
    private $eventDispatcher;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->repository = $productRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function deleteProduct(Product $product): void
    {
        $product->markAsDeleted();
        $this->repository->delete($product);
        $this->eventDispatcher->dispatch(ProductEvents::PRODUCT_DELETED, new ProductEvent($product));
    }

}