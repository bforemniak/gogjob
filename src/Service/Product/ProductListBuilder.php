<?php

namespace App\Service\Product;

use App\Model\Product\Repository\ProductListRepositoryInterface;
use App\Model\Product\Repository\ViewModel\ProductList;

class ProductListBuilder
{
    private $productListRepository;

    public function __construct(ProductListRepositoryInterface $productListRepository)
    {
        $this->productListRepository = $productListRepository;
    }

    public function getProductList(int $limit, int $offset): ProductList
    {
        return new ProductList(
            $this->productListRepository->findProducts($limit, $offset),
            $this->productListRepository->getProductsTotalCount()
        );
    }
}