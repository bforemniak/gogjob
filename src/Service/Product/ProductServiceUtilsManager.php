<?php

namespace App\Service\Product;

use App\EventListener\Events\ProductEvent;
use App\EventListener\Events\ProductEvents;
use App\Model\Product\Exception\ProductTitleAlreadyExist;
use App\Model\Product\Product;
use App\Repository\ProductRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProductServiceUtilsManager //place were i give up
{
    private $productCreateRepository;
    private $titleValidator;
    private $eventDispatcher;

    public function __construct(
        ProductRepository $productCreateRepository,
        ProductUniqueTitleValidator $titleValidator,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->productCreateRepository = $productCreateRepository;
        $this->titleValidator = $titleValidator;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function addProduct(Product $product): void
    {
        if (!$this->titleValidator->isTitleUnique($product->getTitle())) {
            throw ProductTitleAlreadyExist::createForProductAdd($product->getTitle());
        }
        $this->productCreateRepository->addProduct($product);

        $this->eventDispatcher->dispatch(ProductEvents::PRODUCT_CREATED, new ProductEvent($product));
    }


    public function editProduct(Product $product): void
    {
        if (!$this->titleValidator->isTitleUnique($product->getTitle())) {
            throw ProductTitleAlreadyExist::createForProductEdit($product->getTitle());
        }
        $this->productCreateRepository->addProduct($product);

        $this->eventDispatcher->dispatch(ProductEvents::PRODUCT_EDITED, new ProductEvent($product));
    }

}