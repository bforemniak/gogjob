<?php

namespace App\Service\Product;

use App\Model\Product\Properties\ProductTitle;
use App\Model\Product\Repository\FindProductByTitleRepositoryInterface;

class ProductUniqueTitleValidator
{
    private $findProductByTitleRepository;

    public function __construct(FindProductByTitleRepositoryInterface $findProductByTitleRepository)
    {
        $this->findProductByTitleRepository = $findProductByTitleRepository;
    }

    /**
     * @param ProductTitle $title
     * @return bool
     */
    public function isTitleUnique(ProductTitle $title): bool
    {
        return !$this->findProductByTitleRepository->findProduct($title);
    }
}