<?php

namespace App\Service\Product\Form;

use App\Form\FormModel\ProductParams;
use App\Form\ProductParamsFormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class ProductFormFactory
{
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function createProductAddForm(ProductParams $params): FormInterface
    {
        return $this->formFactory->create(ProductParamsFormType::class, $params);
    }

    public function createProductEditForm(ProductParams $params): FormInterface
    {
        return $this->formFactory->create(ProductParamsFormType::class, $params);
    }


}