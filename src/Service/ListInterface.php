<?php

namespace App\Service;

interface ListInterface
{
    public function getTotalCount(): int;

    public function getItems(): array;
}