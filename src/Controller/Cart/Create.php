<?php

namespace App\Controller\Cart;

use App\Http\Response\CreateResponse;
use App\Model\Cart\Cart;
use App\Model\Cart\Repository\CartRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class Create
{
    private $createRepository;

    public function __construct(CartRepositoryInterface $createRepository)
    {
        $this->createRepository = $createRepository;
    }


    public function __invoke(): Response
    {
        $cart = new Cart();
        $this->createRepository->add($cart);

        return new CreateResponse($cart->getId());
    }
}