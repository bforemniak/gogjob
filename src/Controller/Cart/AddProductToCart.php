<?php

namespace App\Controller\Cart;

use App\Http\Response\EditResponse;
use App\Model\Cart\Cart;
use App\Model\Cart\Repository\CartRepositoryInterface;
use App\Model\Product\Product;
use Symfony\Component\HttpFoundation\Response;

class AddProductToCart
{
    private $repository;

    public function __construct(CartRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function __invoke(Cart $cart, Product $product): Response
    {
        $cart->addProductToCart($product);
        $this->repository->add($cart);

        return new EditResponse();
    }
}