<?php

namespace App\Controller\Product;

use App\Form\FormModel\ProductParams;
use App\Http\Response\CreateResponse;
use App\Http\Response\FormValidationResponse;
use App\Model\Product\Product;
use App\Model\Product\Repository\ProductRepositoryInterface;
use App\Service\Product\Form\ProductFormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Create
{
    private $createRepository;
    private $formFactory;

    public function __construct(ProductRepositoryInterface $createRepository, ProductFormFactory $formFactory)
    {
        $this->createRepository = $createRepository;
        $this->formFactory = $formFactory;
    }

    public function __invoke(Request $request): Response
    {
        $createParams = new ProductParams();
        $form = $this->formFactory->createProductEditForm($createParams);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return new FormValidationResponse($form);
        }

        $product = new Product($createParams->getProductTitle(), $createParams->getProductPrice());
        $this->createRepository->addProduct($product);

        return new CreateResponse($product->getProductId());
    }
}