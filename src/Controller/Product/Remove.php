<?php

namespace App\Controller\Product;

use App\Http\Response\RemoveResponse;
use App\Model\Product\Product;
use App\Service\Product\ProductRemover;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Remove
{
    private $remover;

    public function __construct(ProductRemover $remover)
    {
        $this->remover = $remover;
    }

    public function __invoke(Request $request, Product $product): Response
    {
        $this->remover->deleteProduct($product);
        return new RemoveResponse();
    }
}