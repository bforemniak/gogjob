<?php

namespace App\Controller\Product;

use App\Http\Response\ListResponse;
use App\Service\Product\ProductListBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Index
{
    private $listBuilder;

    public function __construct(ProductListBuilder $builder)
    {
        $this->listBuilder = $builder;
    }

    public function __invoke(Request $request): Response
    {
        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 3);

        return new ListResponse($this->listBuilder->getProductList($limit, $offset));
    }
}