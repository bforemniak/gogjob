<?php

namespace App\Controller\Product;

use App\Form\FormModel\ProductParams;
use App\Http\Response\EditResponse;
use App\Http\Response\FormValidationResponse;
use App\Model\Product\Product;
use App\Service\Product\Form\ProductFormFactory;
use App\Service\Product\ProductServiceUtilsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Edit
{
    private $productManager;
    private $formFactory;

    public function __construct(ProductServiceUtilsManager $manager, ProductFormFactory $formFactory)
    {
        $this->productManager = $manager;
        $this->formFactory = $formFactory;
    }

    public function __invoke(Request $request, Product $product): Response
    {
        $params = ProductParams::createFromProduct($product);
        $form = $this->formFactory->createProductEditForm($params);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return new FormValidationResponse($form);
        }

        $product->update($params);
        $this->productManager->editProduct($product);

        return new EditResponse();
    }
}