<?php

namespace App\Form\Constraint;

use Symfony\Component\Validator\Constraint;

class ProductTitleConstraint extends Constraint
{
    public $message = 'Product title Already exists';
}
