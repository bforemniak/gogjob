<?php

namespace App\Form\Constraint;

use App\Model\Product\Properties\ProductTitle;
use App\Service\Product\ProductUniqueTitleValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProductTitleConstraintValidator extends ConstraintValidator
{
    private $validator;

    public function __construct(ProductUniqueTitleValidator $validator)
    {
        $this->validator = $validator;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ProductTitleConstraint) {
            throw new \InvalidArgumentException();
        }

        $title = new ProductTitle($value);

        if (!$this->validator->isTitleUnique($title)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
