<?php

namespace App\Form;

use App\Form\Constraint\ProductTitleConstraint;
use App\Form\FormModel\ProductParams;
use App\Model\Product\Properties\ProductTitle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductParamsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('title', TextType::class, [
            'constraints' => [
                new NotBlank(),
                new Length(['min' => 3, 'max' => 512]),
//                new ProductTitleConstraint() to trzebaby przerobic
            ]
        ]);

        $builder->add('price', NumberType::class, [
            'scale' => 2,
            'constraints' => [
                new NotBlank(),
                new GreaterThan(0)
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => ProductParams::class,
            'method' => 'POST'
        ]);
    }

    public function getBlockPrefix(): ?string
    {
        return '';
    }

}