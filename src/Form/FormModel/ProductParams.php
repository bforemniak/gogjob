<?php

namespace App\Form\FormModel;

use App\Model\Product\Product;
use App\Model\Product\Properties\ProductPrice;
use App\Model\Product\Properties\ProductTitle;

class ProductParams
{
    public $title;
    public $price;

    public function getProductPrice(): ProductPrice
    {
        return new ProductPrice($this->price);
    }

    public function getProductTitle(): ProductTitle
    {
        return new ProductTitle($this->title);
    }

    public static function createFromProduct(Product $product): ProductParams
    {
        $self = new self();
        $self->title = $product->getTitle()->getTitle();
        $self->price = $product->getPrice()->getPrice();
        return $self;
    }
}