<?php

namespace App\Model\Product\Repository;

interface ProductListRepositoryInterface
{
    public function findProducts(int $limit = 3, int $offset = 0): array;

    public function getProductsTotalCount(): int;
}