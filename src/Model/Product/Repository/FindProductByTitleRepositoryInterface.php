<?php

namespace App\Model\Product\Repository;

use App\Model\Product\Product;
use App\Model\Product\Properties\ProductTitle;

interface FindProductByTitleRepositoryInterface
{
    public function findProduct(ProductTitle $title): ?Product;
}