<?php

namespace App\Model\Product\Repository;

use App\Model\Product\Exception\ProductTitleAlreadyExist;
use App\Model\Product\Product;

interface ProductRepositoryInterface
{
    /**
     * @param Product $product
     * @throws ProductTitleAlreadyExist
     */
    public function addProduct(Product $product): void;

    /**
     * @param Product $product
     * @throws ProductTitleAlreadyExist
     */
    public function editProduct(Product $product): void;

    public function delete(Product $product): void;
}