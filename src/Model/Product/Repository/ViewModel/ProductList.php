<?php

namespace App\Model\Product\Repository\ViewModel;

use App\Service\ListInterface;

class ProductList implements ListInterface
{
    public $totalCount;
    public $items;

    public function __construct(array $items, int $totalCount)
    {
        $this->totalCount = $totalCount;
        $this->items = $items;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}