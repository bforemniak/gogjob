<?php

namespace App\Model\Product;

use App\Form\FormModel\ProductParams;

final class Product extends BaseProduct
{

    public function update(ProductParams $params): void
    {
        $this->setTitle($params->getProductTitle());
        $this->price = $params->getProductPrice();
    }

    public function markAsDeleted(): void
    {
        if (null !== $this->deletedAt) {
            throw new \InvalidArgumentException('Product already deleted');
        }

        $this->deletedAt = new \DateTime();
    }

}