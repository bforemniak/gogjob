<?php

namespace App\Model\Product;

use App\Model\Product\Properties\ProductId;
use App\Model\Product\Properties\ProductPrice;
use App\Model\Product\Properties\ProductTitle;
use Ramsey\Uuid\Uuid;

abstract class BaseProduct
{
    protected $id;
    protected $price;
    protected $title;
    protected $titleHash;
    protected $createdAt;
    protected $deletedAt;

    public function __construct(ProductTitle $title, ProductPrice $price)
    {
        $this->id = Uuid::uuid4();

        $this->setTitle($title);
        $this->price = $price->getPrice();
        $this->createdAt = new \DateTime();
    }

    protected function setTitle(ProductTitle $title): void
    {
        $this->title = $title->getTitle();
        $this->titleHash = $title->getTitleHash();
    }

    public function getProductId(): ProductId
    {
        return new ProductId($this->id);
    }

    public function getTitle(): ProductTitle
    {
        return new ProductTitle($this->title);
    }

    public function getPrice(): ProductPrice
    {
        return new ProductPrice($this->price);
    }


}