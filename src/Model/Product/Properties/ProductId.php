<?php

namespace App\Model\Product\Properties;

use Ramsey\Uuid\Uuid;

final class ProductId
{
    private $id;

    public function __construct(string $id)
    {
        if (!Uuid::isValid($id)) {
            throw new \InvalidArgumentException('Price must be greater than 0');
        }

        $this->id = $id;
    }

    public function isEqual(ProductId $productId): bool
    {
        return $this->id === $productId->id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

}