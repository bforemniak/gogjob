<?php

namespace App\Model\Product\Properties;

class ProductPrice
{
    private $price;

    public function __construct(float $price)
    {
        if ($price <= 0.0) {
            throw new \InvalidArgumentException('Price must be greater than 0');
        }

        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function __toString(): string
    {
        return number_format($this->price, 2) . ' USD';
    }
}