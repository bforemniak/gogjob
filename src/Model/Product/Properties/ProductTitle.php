<?php

namespace App\Model\Product\Properties;

class ProductTitle
{
    private $title;

    public function __construct(string $title)
    {
        self::isValid($title);

        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public static function isValid(string $title): bool
    {
        if (\strlen($title) < 3) {
            throw new \InvalidArgumentException('Product name must be at least 3 characters long');
        }

        if (\strlen($title) > 512) {
            throw new \InvalidArgumentException('Product name must be max 512 characters long');
        }

        return true;
    }

    public function getTitleHash()
    {
        return sha1($this->title);
    }

    public function __toString(): string
    {
        return $this->title;
    }

}