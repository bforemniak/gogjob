<?php

namespace App\Model\Product\Exception;

use App\Model\Product\Properties\ProductTitle;

class ProductTitleAlreadyExist extends \Exception
{

    public static function createForProductAdd(ProductTitle $title)
    {
        return new self('Can not create product with title  "' . $title . '"" already exists');
    }

    public static function createForProductEdit(ProductTitle $title)
    {
        return new self('Can not create product with title, "' . $title . '"" already exists');
    }

}