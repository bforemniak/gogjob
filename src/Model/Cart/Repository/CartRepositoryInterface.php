<?php

namespace App\Model\Cart\Repository;

use App\Model\Cart\Cart;

interface CartRepositoryInterface
{
    public function add(Cart $cart): void;
}