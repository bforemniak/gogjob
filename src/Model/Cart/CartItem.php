<?php

namespace App\Model\Cart;

use App\Model\Cart\Exception\WrongCartItemProduct;
use App\Model\Cart\Properties\CartItemQuantity;
use App\Model\Product\Product;

final class CartItem
{
    private $id;
    private $quantity;
    private $product;
    private $cart;

    public function __construct(Cart $cart, Product $product)
    {
        $this->cart = $cart;
        $this->product = $product;
        $this->quantity = 1;
    }

    public function containProduct(Product $product): bool
    {
        return $this->product->getProductId()->isEqual($product->getProductId());
    }

    public function getQuantity(): CartItemQuantity
    {
        return new CartItemQuantity($this->quantity);
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function addAnotherElement(Product $product): void
    {
        if (!$this->containProduct($product)) {
            throw new WrongCartItemProduct();
        }

        $this->quantity = $this->getQuantity()->increaseQuantity()->getQuantity();
    }

    public function removeProductElement(Product $product): void
    {
        if (!$this->containProduct($product)) {
            throw new WrongCartItemProduct();
        }

        $this->quantity = $this->getQuantity()->decreaseQuantity()->getQuantity();
    }

    public function isEmpty(): bool
    {
        return $this->getQuantity()->isZero();
    }
}