<?php

namespace App\Model\Cart;

use App\Model\Cart\Exception\CartMaxQuantityExceeded;
use App\Model\Product\Product;

final class Cart extends BaseCart
{
    public function addProductToCart(Product $product): void
    {
        if ($this->isProductAlreadyInCart($product)) {
            $this->addAnotherProductAlreadyExistedInCart($product);
            return;
        }

        if (!$this->isPlaceInCart()) {
            throw new CartMaxQuantityExceeded();
        }

        $this->items [] = new CartItem($this, $product);
    }


}