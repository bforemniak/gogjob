<?php

namespace App\Model\Cart;

use App\Model\Cart\Exception\CartItemWithProductNotFound;
use App\Model\Cart\Properties\CartId;
use App\Model\Product\Product;
use Ramsey\Uuid\Uuid;

abstract class BaseCart
{
    private const MAX_PRODUCT_NUM = 3;

    protected $id;
    protected $createdAt;
    /** @var CartItem[] */
    protected $items = [];

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->createdAt = new \DateTime();
    }

    public function getId(): CartId
    {
        return new CartId($this->id);
    }

    protected function isPlaceInCart(): bool
    {
        return \count($this->items) <= self::MAX_PRODUCT_NUM;
    }

    protected function addAnotherProductAlreadyExistedInCart(Product $product): void
    {
        $this->getItemWithProduct($product)->addAnotherElement($product);
    }

    protected function getItemWithProduct(Product $product): CartItem
    {
        foreach ($this->items as $item) {
            if ($item->containProduct($product)) {
                return $item;
            }
        }

        throw new CartItemWithProductNotFound();
    }

    protected function isProductAlreadyInCart(Product $product): bool
    {
        foreach ($this->items as $item) {
            if ($item->containProduct($product)) {
                return true;
            }
        }

        return false;
    }

}