<?php

namespace App\Model\Cart\Exception;

class CartItemMaxQuantityExceeded extends \Exception
{
    public function __construct()
    {
        parent::__construct('to many units of same product');
    }
}