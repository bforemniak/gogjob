<?php

namespace App\Model\Cart\Exception;

class CartMaxQuantityExceeded extends \Exception
{
    public function __construct()
    {
        parent::__construct('cart max quantity exceeded');
    }
}