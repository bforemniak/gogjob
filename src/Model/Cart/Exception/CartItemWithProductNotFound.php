<?php

namespace App\Model\Cart\Exception;

class CartItemWithProductNotFound extends \Exception
{
}