<?php

namespace App\Model\Cart\Properties;

use Ramsey\Uuid\Uuid;

final class CartId
{
    private $id;

    public function __construct(string $id)
    {
        if (!Uuid::isValid($id)) {
            throw new \InvalidArgumentException('invalid uuid');
        }

        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

}