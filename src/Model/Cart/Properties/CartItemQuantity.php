<?php

namespace App\Model\Cart\Properties;

use App\Model\Cart\Exception\CartItemMaxQuantityExceeded;

class CartItemQuantity
{
    private const MAXIMUM_QUANTITY = 3;

    private $quantity;

    public function __construct(int $quantity)
    {
        if ($quantity < 0) {
            throw new \InvalidArgumentException('quantity must be positive integer');
        }

        if ($quantity > self::MAXIMUM_QUANTITY) {
            throw new CartItemMaxQuantityExceeded();
        }

        $this->quantity = $quantity;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function increaseQuantity(): CartItemQuantity
    {
        if ($this->quantity === self::MAXIMUM_QUANTITY) {
            throw new CartItemMaxQuantityExceeded();
        }

        return new CartItemQuantity($this->quantity + 1);
    }

    public function decreaseQuantity(): CartItemQuantity
    {
        if ($this->quantity === 0) {
            throw new \InvalidArgumentException('quantity can be lower than 0');
        }

        $this->quantity--;

        return new CartItemQuantity($this->quantity - 1);
    }

    public function __toString()
    {
        return (string)$this->quantity;
    }

    public function isZero(): bool
    {
        return $this->quantity === 0;
    }
}