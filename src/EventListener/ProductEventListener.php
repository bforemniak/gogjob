<?php

namespace App\EventListener;

use App\EventListener\Events\ProductEvent;
use App\EventListener\Events\ProductEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ProductEventListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            ProductEvents::PRODUCT_DELETED => 'onProductDeleted'
        ];
    }


    public function onProductDeleted(ProductEvent $event)
    {
        // handle cart with deleted product problem
    }
}