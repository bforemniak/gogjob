<?php

namespace App\EventListener\Events;

use App\Model\Product\Product;
use Symfony\Component\EventDispatcher\Event;

class ProductEvent extends Event
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

}