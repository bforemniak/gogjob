<?php

namespace App\EventListener\Events;

class ProductEvents
{
    public const PRODUCT_CREATED = 'product.create';
    public const PRODUCT_EDITED = 'product.edit';
    public const PRODUCT_DELETED = 'product.delete';
}