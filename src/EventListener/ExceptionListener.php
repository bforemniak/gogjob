<?php

namespace App\EventListener;

use App\Model\Cart\Exception\CartItemMaxQuantityExceeded;
use App\Model\Cart\Exception\CartMaxQuantityExceeded;
use App\Model\Product\Exception\ProductTitleAlreadyExist;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionListener implements EventSubscriberInterface
{
    private static $exceptionsToHandle = [
        ProductTitleAlreadyExist::class,
        CartMaxQuantityExceeded::class,
        CartItemMaxQuantityExceeded::class,
    ];

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['handleException', 5]
        ];
    }

    public function handleException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($this->shouldExceptionBeHandled($exception)) {
            $event->setResponse(new JsonResponse(
                ['error' => $exception->getMessage()], JsonResponse::HTTP_BAD_REQUEST
            ));
            $event->stopPropagation();
        }
    }


    private function shouldExceptionBeHandled($exception): bool
    {
        return \in_array(\get_class($exception), self::$exceptionsToHandle, true);
    }
}