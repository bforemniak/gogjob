<?php

namespace App\Http\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class EditResponse extends JsonResponse
{
    public function __construct()
    {
        parent::__construct(null, self::HTTP_OK);
    }

}