<?php

namespace App\Http\Response;

use App\Service\ListInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ListResponse extends JsonResponse
{
    public function __construct(ListInterface $list)
    {
        $headers = [
            'X-Total-Count' => $list->getTotalCount()
        ];

        parent::__construct($list->getItems(), self::HTTP_OK, $headers);
    }

}