<?php

namespace App\Http\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class CreateResponse extends JsonResponse
{
    public function __construct($id)
    {
        parent::__construct(['id' => (string)$id], self::HTTP_CREATED);
    }

}