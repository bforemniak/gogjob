<?php

namespace App\Http\Response;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class FormValidationResponse extends JsonResponse
{
    public function __construct(FormInterface $form)
    {
        $message = (string)$form->getErrors(true);
        parent::__construct($message, self::HTTP_BAD_REQUEST);
    }

}