<?php

namespace App\Repository;

use App\Model\Product\Exception\ProductTitleAlreadyExist;
use App\Model\Product\Product;
use App\Model\Product\Repository\ProductRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;

class ProductRepository implements ProductRepositoryInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Product $product
     * @throws ProductTitleAlreadyExist
     */
    public function addProduct(Product $product): void
    {
        try {
            $this->manager->persist($product);
            $this->manager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw ProductTitleAlreadyExist::createForProductAdd($product->getTitle());
        }
    }

    /**
     * @param Product $product
     * @throws ProductTitleAlreadyExist
     */
    public function editProduct(Product $product): void
    {
        try {
            $this->manager->persist($product);
            $this->manager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw ProductTitleAlreadyExist::createForProductAdd($product->getTitle());
        }
    }

    public function delete(Product $product): void
    {
        $this->manager->persist($product);
        $this->manager->flush();
    }
}