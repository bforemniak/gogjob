<?php

namespace App\Repository;

use App\Model\Cart\Cart;
use App\Model\Cart\Repository\CartRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class CartRepository implements CartRepositoryInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    public function add(Cart $cart): void
    {
        $this->manager->persist($cart);
        $this->manager->flush();
    }
}