<?php

namespace App\Repository;

use App\Model\Product\Product;
use App\Model\Product\Repository\ProductListRepositoryInterface;
use App\Model\Product\Repository\ViewModel\ProductView;
use Doctrine\ORM\EntityManagerInterface;

class ProductListRepository implements ProductListRepositoryInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function findProducts(int $limit = 3, int $offset = 0): array
    {
        return $this->manager->createQueryBuilder()
            ->select('new ' . ProductView::class . '(p.id, p.title, p.price)')
            ->from(Product::class, 'p')
            ->where('p.deletedAt is null')
            ->getQuery()
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();
    }

    public function getProductsTotalCount(): int
    {
        return (int)$this->manager->createQueryBuilder()
            ->select('count(p.id)')
            ->from(Product::class, 'p')
            ->getQuery()
            ->getSingleScalarResult();
    }
}