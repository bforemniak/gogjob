<?php

namespace App\Repository;

use App\Model\Product\Product;
use App\Model\Product\Properties\ProductTitle;
use App\Model\Product\Repository\FindProductByTitleRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class FindProductByTitleRepository implements FindProductByTitleRepositoryInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function findProduct(ProductTitle $title): ?Product
    {
        return $this->manager->createQueryBuilder()
            ->select('p')
            ->from(Product::class, 'p')
            ->where('p.titleHash = :hash')->setParameter('hash', $title->getTitleHash())
            ->getQuery()
            ->getOneOrNullResult();
    }

}